# TINlab

school projects concerning tinlab 1 (Advances Algoritms) and tinlab 2 (tba)

### Https
#### Setup virtual host so Certbot can verify you control the given domain
in /etc/httpd/conf/httpd.conf uncomment:
```apache
Include conf/extra/httpd-vhosts.conf
```
and:
```apache
LoadModule rewrite_module modules/mod_rewrite.so    
```
in /etc/httpd/conf/extra/httpd-vhosts.conf add your virtual host, eg:
```apache
<VirtualHost *:80>
    ServerAdmin gponson96@hotmail.com
    DocumentRoot "/home/ento/website"
    ServerName www.smartentofarming.nl
    ErrorLog "/home/ento/website/smartEntofarming-error_log"
    CustomLog "/home/ento/website/smartEntofarming-access_log" common
</VirtualHost>
```
also add non www url so it can also have https:
```apache
<VirtualHost *:80>
    ServerAdmin gponson96@hotmail.com
    DocumentRoot "/home/ento/website"
    ServerName smartentofarming.nl
    ErrorLog "/home/ento/website/smartEntofarming-error_log"
    CustomLog "/home/ento/website/smartEntofarming-access_log" common
</VirtualHost>
```
#### Install Certbot (https://certbot.eff.org/)
```shell
sudo pacman -S certbot certbot-apache
```
#### Allow SSL certificates in /etc/httpd/conf/httpd.conf:
uncomment
```apache
LoadModule ssl_module modules/mod_ssl.so
```
#### Get and install certificates
```shell
sudo certbot --apache
```
#### Set up automatic renewal:
```shell
echo "0 0,12 * * * root python -c 'import random; import time; time.sleep(random.random() * 3600)' && certbot renew" | sudo tee -a /etc/crontab > /dev/null
```
#### Confirm that Certbot worked
restart apache and check site for https locket
```shell
sudo systemctl restart httpd
```
#### Install fail2ban to ban shady ip's from connecting
to see failed connection attempts:
```shell
journalctl -u sshd | grep Failed
```
install fail2ban:
```shell
sudo pacman -S fail2ban
```
#### Add sshd to a jail
write to a new config file so configs don't get lost on update,
in /etc/fail2ban/jail.local write:
```conf
[DEFAULT]
bantime = 1d

[sshd]
enabled = true
```
start & enable fail2ban:
```shell
sudo systemctl enable --now fail2ban
```
check if fail2ban is running correctly for sshd:
```shell
sudo fail2ban-client status
```
